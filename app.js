const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const session = require('express-session')
const bodyParser = require('body-parser')
const cors = require('cors')
const nodemailer = require('nodemailer');

const CoreAPI = require('./Classes/Core.js')
const Simulacao = require('./Classes/Simulacao.js')
const Mail = require('./Classes/Email.js')

// Planilha Nexen produção
// const spreadsheetKey = '1_o6WtV42gHioYHiOb534LJnx8yC4Me-i'
// const sheetsToExtract = ['Radiacao', 'Tarifas']

// App Defaults
const LayerUrl = 'https://schumann.layer.core.dcg.com.br'
const App = {
  lib: {
    path: './_google-sheets.json',
    spreadsheetKey: '1nES7d5eCQq_qbZo9U6TsywRRuDnlbEByqFaRzCTImfs',
  },
  layer: {
    SearchProduct: `${LayerUrl}/v1/Catalog/API.svc/web/SearchProduct`,
    GetProductPrice: `${LayerUrl}/v1/Catalog/API.svc/web/GetProductPrice`,
    GetProduct: `${LayerUrl}/v1/Catalog/API.svc/web/GetProduct`
  },
  logs: true
}

app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(cors({
  origin: '*'
}))
app.use(cookieParser())
app.use(session({
  secret: App.lib.spreadsheetKey,
  resave: false,
  saveUninitialized: false
}))

const simulator = new Simulacao()
const mail = new Mail()

app.get('/', (req, res) => {
  // Executa a simulação
  //simulator.run(req, res, App)
  res.send('Simulador nexen')
})

app.post('/', (req, res) => {
  // Executa a simulação
  simulator.run(req, res, App)
})

app.post('/update', (req, res) => {
  // Atualiza o arquivo _google-sheets.json com os dados da planilha
  simulator.update(req, res, App)
})

app.post('/catalog', (req, res) => {
  const CORE = new CoreAPI
  CORE
    .SearchProduct(App, {
      "Page": {
        "PageIndex": 0,
        "PageSize": 0
      },
      "Where": "Name.Contains(\"gerador\") == true",
      "WhereMetadata": "SerializedValue.Contains(\"Fibrocimento\") == true",
      "OrderBy": ""
    })
    .then((response) => {
      res.json(response)
      console.log(response)
    })
    .catch((error) => {
      res.json(error)
      console.log(`SearchProduct ${error}`)
    })
})

app.get('/dealerships', (req, res) => {
  simulator.getDealerships(req, res, App)
})

app.post('/places', (req, res) => {
  simulator.getPlaces(req, res, App)
})

app.post('/sendmail', (req, res) => {
  mail.send(req, res)
})

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`running on ${port}...`))