module.exports = function (req, res, next) {
  if (req.session.name === 'schumann.marcelobock') {
    next()
  } else {
    res.redirect('/signin')
  }
}