const axios = require('axios')
const btoa = require('btoa')
const auth = {
  username: 'schumann.marcelobock',
  password: '$JABoyCPO4C6'
}
const Token = btoa(auth.username + ':' + auth.password)
const Authorization = `Basic ${Token}`

axios.defaults.headers.common['Authorization'] = Authorization
module.exports = class CORE {

  SearchProduct(App, Params) {
    // Busca o kit
    return new Promise((resolve, reject) => {

      if (App.logs) console.time('POST: /v1/Catalog/API.svc/web/SearchProduct')

      axios.post(App.layer.SearchProduct, Params)
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })

      if (App.logs) console.timeEnd('POST: /v1/Catalog/API.svc/web/SearchProduct')
    })
  }

  GetProductPrice(App, Params) {
    // Busca o kit
    return new Promise((resolve, reject) => {

      if (App.logs) console.time('POST: /v1/Catalog/API.svc/web/GetProductPrice')

      axios.post(App.layer.GetProductPrice, Params)
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })

      if (App.logs) console.timeEnd('POST: /v1/Catalog/API.svc/web/GetProductPrice')
    })
  }

  GetProduct(App, Params) {
    // Busca o kit
    return new Promise((resolve, reject) => {

      if (App.logs) console.time('POST: /v1/Catalog/API.svc/web/GetProduct')

      axios.post(App.layer.GetProduct, `${Params}`)
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })

      if (App.logs) console.timeEnd('POST: /v1/Catalog/API.svc/web/GetProduct')
    })
  }
}