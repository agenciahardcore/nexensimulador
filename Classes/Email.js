const nodemailer = require('nodemailer');

module.exports = class Mail {
  send(req, res) {

    const html = this.buildHtml(req.body)

    // create reusable transporter object using the default SMTP transportw
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'simulador@nexen.com.br', // generated ethereal user
        pass: 'n3x#n2019' // generated ethereal password
      }
    });

    // send mail with defined transport object
    const mailOptions = {
      from: '"Nexen" <simulador@nexen.com.br>', // sender address
      to: 'simulador@nexen.com.br', // list of receivers
      subject: `Simulação Nexen - ${req.body.nome}`, // Subject line
      text: html, // plain text body
      html: html // html body
    }
    let info = transporter.sendMail(mailOptions, function (err, info) {
      if (err)
        res.send(err)
      else
        res.send(info);
    });
  }

  buildHtml(obj) {
    const url = "https://schumann.core.dcg.com.br/nexen"

    let instalar = obj.instalar === true ? 'SIM' : 'NÃO'
    let credenciado = obj.credenciado === true ? 'SIM' : 'NÃO'
    let credenciar = obj.credenciar === true ? 'SIM' : 'NÃO'

    return `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html style="width:100%;font-family:'Montserrat', Helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
    <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="telephone=no" name="format-detection">
      <title>Nexen - Simulação</title>
      <!--[if (mso 16)]><style type="text/css">a {text-decoration: none;}</style><![endif]-->
      <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
      <!--[if !mso]>-->
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,900" rel="stylesheet">
      <!--<![endif]-->
      <!-- <style> @media only screen and (max-wifdth: 600px) {p, ul li, ol li, a { font-size: 12px !important } h1 { font-size: 30px !important; text-align: left } h2 { font-size: 26px !important; text-align: left } h3 { font-size: 18px !important; text-align: left } h1 a { font-size: 30px !important; text-align: left } h2 a { font-size: 26px !important; text-align: left } h3 a { font-size: 18px !important; text-align: left } .es-menu td a { font-size: 8px !important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size: 14px !important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size: 12px !important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size: 9px !important } *[class="gmail-fix"] { display: none !important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align: center !important } #input-box { padding-top: 0px; padding-right: 10px; padding-bottom: 10px; padding-left: 40px } #schumann-logo { padding-top: 0px; padding-bottom: 0px } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align: right !important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align: left !important } .es-m-txt-r a img, .es-m-txt-c a img, .es-m-txt-l a img { display: inline !important } .es-button-border { display: block !important } .es-button { font-size: 20px !important; display: block !important; border-left-width: 0px !important; border-right-width: 0px !important } .es-btn-fw { border-width: 10px 0px !important; text-align: center !important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width: 100% !important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width: 100% !important; max-width: 600px !important } .es-adapt-td { display: block !important; width: 100% !important } .adapt-img { width: 100% !important; height: auto !important } .es-m-p0 { padding: 0px !important } .es-m-p0r { padding-right: 0px !important } .es-m-p0l { padding-left: 0px !important } .es-m-p0t { padding-top: 0px !important } .es-m-p0b { padding-bottom: 0 !important } .es-m-p20b { padding-bottom: 20px !important } .es-hidden { display: none !important } table.es-table-not-adapt, .esd-block-html table { width: auto !important } table.es-social { display: inline-block !important } table.es-social td { display: inline-block !important } }</style> -->
      <style>
        #outlook a {
          padding: 0;
        }

        .ExternalClass {
          width: 100%;
        }

        input {
          line-height: 2em;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%;
        }

        .es-button {
          mso-style-priority: 100 !important;
          text-decoration: none !important;
        }

        a[x-apple-data-detectors] {
          color: inherit !important;
          text-decoration: none !important;
          font-size: inherit !important;
          font-family: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
        }

        @-ms-viewport {
          width: device-width;
        }
      </style>
    </head>

    <body style="width:100%;font-family:'Montserrat', Helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
      <div class="es-wrapper-color" style="background-color:#FFFFFF;">

        <table cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td align="center">
              <table cellpadding="1" cellspacing="0" width="600">
                <tr>
                  <td height="80" width="180">
                    <a href="${url}" title="Nexen geradores">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="97px" height="20px">
                      <image  x="0px" y="0px" width="97px" height="20px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAAAUCAQAAAA34JtaAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQEyws9T+YAAADuElEQVRIx7VX62HbOAz+QPb/sRMcO8HB1gBRJrA8QZUJ4kxQZwK7E9iZIM4EUQeQzExQZYKTB5B4P0RSDztukkvwx3zg9QEgBBMAAKxwIWJiq0kDtiSDrL4zFU4Sx2JGbJkUMltSlt+FG0X/tCt7MGYg88INz0RCGrGtyFjTPJjspEUtL5BAWSYF2BJZZ5UA1uIaKamxmK2wLm6PlLFcIR5xls3cuzU13tV60nc1ukcyPo8SuyI9MpDV82HoOJY/xhad1SuTARRd2+Wx+4FtW1z191FiN6e4bdXM2/ixlgZ/AQBMfemdiRLcO87bYunAprQ5abNqLj1IVnLjoZ+keb6jyOIs2ati22VAPAYAB2sAaPo7GJ6Ycuia/VksWkfEvo22fSp4DAqwzygBYgcdMPkkZODxrHdVMxG9/R3m9SSnemKv7FM4/dExSJ+Bg73JVREXcaHtTXtHSrroFls8uLNrjgFALF25HJrER9d6mM+YF7qIizhX8G+Kp8sjZ5/tbX1Zf8upvrS3OHirYhGy0CXYxS0LNX3Zlkgv8fN81/FOl+Rg1l/bwmElTJsdWzYTaLl3Nm6K9ZHM8MXsMGvlim/DLOTUB8QcdJb9LPTIVBQAidgtUif01AcANOvAmXjpxvGSFkvp4/2rCJy4dmc/h30LWy/HGmfIGJ8x0uIlpjobn9DF0EwHN5RdMGsy63oZXYMB9IoIYPYvqtkNdfWCcxYCYAP0Ly/irKLBvq1qAKBVtMIfqVhOE1+KAIC0a5WC/Uo+Rn9WdZIaI722d2p4jZGkt3kYFJ/+SDufCEGk3dpesPosO1/eLtL/UrxMzNRrx6TkBvOwLf1i2I/eR6/OQje7UPwKAEp2fb+lZLrwt01wW7xC14dBAOwv9zs73/AAQCxDH2IvRytmFw4TPk3f/3+BveUtbJ1ZJTdDw6ymqyjp9lFCru8jNVWTeIfFvZezay8qRt2NdXTvoX44hGIb+n8s9tF3ZoDV5GK6Er9pYQOsbnho+5Cp/EeRtHe4WYc8pNGeZ8wAa55NN2KPRL6iab8vC2jSYFhjK/eRlf+KjBakAFLSRVbe+0mqdq7nuzA1pdMU6MMCwHIn95GVv+WO2qE/7t7NB0Mwpo49iCNqJ1dMF362t8vuY1an4Vmv2peU7+zVy7qo/CQIgDG1DvNkn+5qLtYAs59sBxNRf2pS0g3ZxbbmE7oO9rbWwynsPFEYHEozQn7mRokEmtgqGFRk3mJwTKxFDI0YQAY02fCPJyvX2TD+Q9rd/AeWuqLZNd0YEQAAAABJRU5ErkJggg==" />
                      </svg>
                    </a>
                  </td>
                  <td height="80" width="80">
                    <a href="${url}/pesquisa?t=" title="Nexen geradores"
                      style="line-height:18px;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;text-decoration: none;">
                      <strong
                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;line-height:20px;color:#231F20;">Produtos</strong>
                    </a>
                  </td>
                  <td height="80" width="155">
                    <a href="${url}/servicos-e-solucoes" title="Nexen geradores"
                      style="line-height:18px;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;text-decoration: none;">
                      <strong
                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;line-height:20px;color:#231F20;">Serviços
                        e Soluçōes</strong>
                    </a>
                  </td>
                  <td height="80" width="100">
                    <a href="${url}/simulador" title="Nexen geradores"
                      style="line-height:18px;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;text-decoration: none;">
                      <strong
                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'Montserrat', Helvetica, sans-serif;line-height:20px;color:#231F20;">Simulador
                        Solar</strong>
                    </a>
                  </td>

                </tr>
                <tr>
                  <td height="3" style="background-color:#333333;" colspan="4"></td>
                </tr>
                <tr>
                  <td height="40" colspan="4"></td>
                </tr>
                <tr>
                  <td style="padding:20px 0;" colspan="4">
                    <ul style="list-style: none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:'Montserrat', Helvetica, sans-serif;line-height:25px;color:#231F20;">
                      <li>Nome: ${obj.nome}</li>
                      <li>Estado: ${obj.state}</li>
                      <li>Cidade: ${obj.city}</li>
                      <li>Concessionaria: ${obj.concessionaria}</li>
                      <li>Tipo de construção: ${obj.build_type}</li>
                      <li>Tipo de telhado: ${obj.roof_type}</li>
                      <li>Tipo de ligação: ${obj.ligacao}</li>
                      <li>Telefone: ${obj.ddd} + ' ' + ${obj.phone}</li>
                      <li>Fatura em R$: ${obj.fatura}</li>
                      <li>Deseja instalar um gerador solar? <b>${instalar}</b></li>
                      <li>Credenciado Nexen? <b>${credenciado}</b></li>
                      <li>Quer ser credenciado? <b>${credenciar}</b></li>
                      <li>Para visualizar o KIT indicado <a href="https://schumann.core.dcg.com.br/nexen/${obj.Kit.UrlFriendly}-p${obj.Kit.ProductID}">CLIQUE AQUI</a></li>
                      <li>Produto: ${obj.Kit.Name}</li>
                      <li>Preço: ${obj.Kit.price}</li>
                      <li>Potência: ${obj.Kit.GeradorFotovoltaicoPoteciakWp.FormattedValue}</li>
                      <li>Módulos: ${obj.Kit.GeradorFotovoltaicoModulos.FormattedValue}</li>
                      <li>Inversores: ${obj.Kit.GeradorFotovoltaicoInversores.FormattedValue}</li>
                      <li>Altura: ${obj.Kit.NexenAlturacm.FormattedValue}</li>
                      <li>Largura: ${obj.Kit.NexenLarguracm.FormattedValue}</li>
                      <li>Profundidade: ${obj.Kit.NexenProfundidadecm.FormattedValue}</li>
                      <li>Peso: ${obj.Kit.NexenPesokg.FormattedValue}</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td width="300">
                    <p style="width:165px;Margin:40px 0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:'Montserrat', Helvetica, sans-serif;line-height:25px;color:#231F20;">
                      Conecte-se com a nexen:
                    </p>
                  </td>
                  <td colspan="3">
                    <a href="${url}/blog" title="Nexen facebook" style="display: block; float:left;">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="59px" height="59px">
                      <image  x="0px" y="0px" width="59px" height="59px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA7CAAAAADgTq1vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQFx6ATqscAAAAmklEQVRIx+3VsQ2DMBCF4Z/IUijpIjpKb2E2yWzpWIEuJdkg6aiAEaDLBD4rr6DJXXv65GefrKsW5Aq0Kl0v+rG4devW7Zk2mN33a9tJvWKnUc/81DPPB9wiUbsv9DHfK71zLWUejh0Ya1L3s/0AsEFSM1uVt9filaslv0PnB9y7bNt36EnW+gtNgsboW/MtlM/Xrds/tYFVtl+GTRg3y6oERwAAAABJRU5ErkJggg==" />
                      </svg>
                    </a>
                    <a href="${url}/blog" title="Nexen twitter" style="display: block; float:left;">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="59px" height="59px">
                      <image  x="0px" y="0px" width="59px" height="59px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA7CAAAAADgTq1vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQFhfgiSL5AAAA/ElEQVRIx+3WoW7DMBCA4b9TpJrtWFdUw7AOhrWwcGx7hD7Sxja4NxhsWWFgxhLSGGbMQR1oq7WSk9lh03zId9KnO5+JR3sGR8J0KK1vhrcl2mijjfav2MRVtOp82n0iC2kQb/v8dHc8vFZAIYe198zN15sBIK8ArJnuvK3CvmwtUJ4Kh8z7vmpWsdncazGnwoP/rprMtJDn/BKumeW9vcpvAyzz6zQNsavJcKsyfZHNdIhFlRfJMmRXkD7+7Gehwyzp+izmnW2d72taWxQWgHHWTZ1WPorjA4/TpXRTRnvnX6U0FhFNX9SJu677We+uvCLaaKP9nzahHmy/AZIZNuocYNNZAAAAAElFTkSuQmCC" />
                      </svg>
                    </a>
                    <a href="${url}/blog" title="Nexen instagram" style="display: block; float:left;">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="59px" height="59px">
                      <image  x="0px" y="0px" width="59px" height="59px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA7CAAAAADgTq1vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQFwkDnS7bAAABKElEQVRIx+2ULVMDMRCG3zI30zjqOFTjro7Ic8jWUVccSH4Gkr+AYnDgikNyrqgGF1zVJXWpulQVwRXKzOboxTDMZNUmeZ/Z7EfSKRFsCY5DUX0QHhaRjWxkI9vWEs++fDVbl/Fhj9R0SvKvm771+dZ3aj3hhEaj3BA2v57vrKq7m4oQlXS+s7748p1hY6f2r9WyvqOxgJyi17VtagUAsrDgwzwHUvK8oUfq6ejirLp3XkED+5ydc3G5kSHsKgfAUhXCwgCAZSHsSWGAl1XuFTTUeWRuubWnPIRlV3JxOEkBwLVgu59iIer1crB/vvz9R6QZWrCj6tHuoEVGDpbnDS4e1qzWO4NsTDVKe1g49R14QI+z9tWZCfxq//GfjGxkI/u3bAIdzH4AZ8GCAVJ2sI8AAAAASUVORK5CYII=" />
                      </svg>
                    </a>
                    <a href="${url}/blog" title="Nexen youtube" style="display: block; float:left;">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="59px" height="59px">
                      <image  x="0px" y="0px" width="59px" height="59px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA7CAAAAADgTq1vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQFTaHzWFkAAAArklEQVRIx2N8zkA2YGGQIFfrCybyrWUY1Tuqd1TvqN6RoJcFq+iPQw9eIHgKBnok6N11AZn34MF/feLdfB2N/5AE//5kYGBgYJA3gPHfkxxWCn55GvjCCn84C4TFk62X4cNFPJIs+HT+OHWAgUy9J0/+gLI4SXCzPMTaH6h84vQ6oHLZ1bGqYnyOtd7/cPEBgqOgL4BNzQsceokBo22GUb2jekf1jgi9LAwvyNYLAJ88Ib+Anms+AAAAAElFTkSuQmCC" />
                      </svg>
                    </a>
                    <a href="${url}/blog" title="Nexen linkedin" style="display: block; float:left;">
                      <svg 
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns:xlink="http://www.w3.org/1999/xlink"
                      width="59px" height="59px">
                      <image  x="0px" y="0px" width="59px" height="59px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA7CAAAAADgTq1vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjDBIQFixRgsvdAAAAyUlEQVRIx+3WrQ7CMBDA8f/IRN2QQ22PULk3AIdE8mrgkEziwCFncZvZh6s8iSEZomTLCZIlvaSmzS93bXNNoxZ1xGy0tFvp0xJssMEu18a+ybp0dmt0ea+OqlLWLJ+hsTtIimkbtb53Uvp8mnbes8KMVAYxmR977amBY35/QGZvAutDOnu/YwylAO7sFFZIAOQ5u+av2FsuL2gUeTMLBdArLED+a2GJPbhE6+2jXiA1zoFJgRrvRXX+HpwV4a8SbLDB/tXGdGr7BhIXLi8NcFfiAAAAAElFTkSuQmCC" />
                      </svg>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td height="3" style="background-color:#333333;" colspan="4"></td>
                </tr>
                <tr>
                  <td colspan="4" style="padding:60px 80px;">
                    <p
                      style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color:#767676;">
                      Para esclarecer dúvidas ou enviar sugestões, acesse nossa <a href="Central"
                        style="font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color: #231F20;">Central
                        de relacionamento</a>.
                    </p><br>
                    <p
                      style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color:#767676;">
                      Validade do E-mail
                    </p><br>
                    <p
                      style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color:#767676;">
                      As ofertas anunciadas neste e-mail são válidas até 27/08/2018 às 23:59, ou enquanto durarem os
                      estoques.
                    </p><br>
                    <p
                      style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color:#767676;">
                      Preços e Condições
                    </p><br>
                    <p
                      style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:'Montserrat', Helvetica, sans-serif;line-height:23px;color:#767676;">
                      Preços sujeitos à alteração e exclusivos para este canal de comunicação. Para ativar determinados
                      preços e promoções é necessário clicar neste e-mail. Compras feitas com cartões de crédito podem ser
                      parceladas em até 10x sem juros. Caso haja diferença de preço ou descrição do produto entre este
                      e-mail e o site, a condição válida e praticada será a do site.
                    </p>
                  </td>
                </tr>

              </table>
            </td>
          </tr>
        </table>
      </div>
    </body>
    </html>
    `
  }
}

// year: "2019-12-12T21:08:10.641Z"
// eficiencia: 85
// potencia_mod: 330
// madeira: 0.531
// ton_co2: 7.14
// fatura: 1444
// tarifa: 0.62
// media_radiacao: 4.62
// media_dias: 30
// consumo: 2026.26
// consumo_comp: 1976.26
// geracao: 38.88
// qtd_mod: 51
// pot_sistema: 16.83
// co2: 315872.78
// arvores: 2256
// economia_mensal: 1229.39
// Kit: {Name: "Gerador FV nexen Fibrocimento 16.83 kWp", ProductID: 1070493,…}
// chart: [152048.3, 133261.11, 140087.9, 127001.08, 108468.45, 91399.27, 100522.35, 113582.55, 105874.15,…]
// retorno_investimento: 4.02878663402175