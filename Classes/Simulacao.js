const { extractSheets } = require('spreadsheet-to-json')
const fs = require('fs')
const _ = require('lodash')
const path = require('path')
const fixUtf8 = require('fix-utf8')
const credentials = require('../Credentials/_google-generated-creds.json')
const CoreAPI = require('./Core.js')

module.exports = class Simulacao {

  constructor(
    arvores = null,
    co2 = null,
    consumo = null,
    consumo_comp = null,
    economia_mensal = null,
    geracao = null,
    media_dias = null,
    media_radiacao = null,
    pot_sistema = null,
    qtd_mod = null,
    retorno_do_investimento = null,
  ) {

    this.arvores = arvores
    this.co2 = co2
    this.consumo = consumo
    this.consumo_comp = consumo_comp
    this.economia_mensal = economia_mensal
    this.geracao = geracao
    this.media_dias = media_dias
    this.media_radiacao = media_radiacao
    this.pot_sistema = pot_sistema
    this.qtd_mod = qtd_mod
    this.retorno_do_investimento = retorno_do_investimento
    this.chart = []
  }

  run(req, res, App) {
    console.time('Run Simulacao')
    console.log('Busca formulário', JSON.stringify(req.body, null, 2))

    const CORE = new CoreAPI

    extractSheets({
      credentials,
      spreadsheetKey: App.lib.spreadsheetKey,
      sheetsToExtract: App.lib.sheetsToExtract
    }, (err, data) => {
      if (err) throw err

      const db = data
      const Plan = db.Radiacao.find(plan => plan.NAME.normalize("NFD").replace(/[\u0300-\u036f]/g, "") === req.body.city.normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
      const Months = [
        Number(Plan.JAN.replace(',', '.')),
        Number(Plan.FEB.replace(',', '.')),
        Number(Plan.MAR.replace(',', '.')),
        Number(Plan.APR.replace(',', '.')),
        Number(Plan.MAY.replace(',', '.')),
        Number(Plan.JUN.replace(',', '.')),
        Number(Plan.JUL.replace(',', '.')),
        Number(Plan.AUG.replace(',', '.')),
        Number(Plan.SEP.replace(',', '.')),
        Number(Plan.OCT.replace(',', '.')),
        Number(Plan.NOV.replace(',', '.')),
        Number(Plan.DEC.replace(',', '.'))
      ].map(Number)

      let Response

      const Tax = db.Tarifas.find(tax => tax.CONCESSIONARIA.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase() === req.body.concessionaria.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase() && tax.TIPO.toLowerCase() === req.body.build_type.toLowerCase())
      const RoofType = req.body.roof_type

      if (Plan !== undefined && Tax !== undefined) {
        const year = new Date()
        const eficiencia = 85
        const potencia_mod = 330
        const madeira = 0.531
        const ton_co2 = 7.14

        const fatura = Number(parseFloat(req.body.fatura.replace('.', '')).toFixed(2))
        const tarifa = Number(parseFloat(Tax.TARIFA.replace(',', '.')).toFixed(4))
        const ligacao = parseFloat(req.body.ligacao)

        // Medias
        // this.media_radiacao = Number(Plan.ANNUAL)
        this.media_radiacao = Number(this.average(Months))
        this.media_dias = Number(parseFloat((this.totalDays(year.getUTCFullYear()) / 12).toFixed(0)))

        this.consumo = parseFloat((this.percentage(fatura, 87) / tarifa).toFixed(2))
        this.consumo_comp = parseFloat((this.consumo - ligacao).toFixed(2))
        this.geracao = parseFloat(((this.percentage(potencia_mod, eficiencia) * this.media_radiacao * this.media_dias) / 1000).toFixed(2)) //41465.62
        this.qtd_mod = Math.ceil(this.consumo_comp / this.geracao)
        this.pot_sistema = (this.qtd_mod * potencia_mod) / 1000

        this.co2 = parseFloat((madeira * this.geracao * this.qtd_mod * 12 * 25).toFixed(2))
        this.arvores = Math.ceil((this.co2 * ton_co2) / 1000)
        this.economia_mensal = parseFloat((this.geracao * this.qtd_mod * tarifa).toFixed(2))

        console.log(this.qtd_mod)

        //   Response = this
        Response = {
          nome: req.body.nome,
          state: req.body.state,
          city: req.body.city,
          concessionaria: req.body.concessionaria,
          build_type: req.body.build_type,
          roof_type: req.body.roof_type,
          ligacao: req.body.ligacao,
          phone: `(${req.body.ddd}) ${req.body.phone}`,
          fatura: req.body.fatura,
          year,
          eficiencia,
          potencia_mod,
          madeira,
          ton_co2,
          fatura,
          tarifa,
          media_radiacao: this.media_radiacao,
          media_dias: this.media_dias,
          consumo: this.consumo,
          consumo_comp: this.consumo_comp,
          geracao: this.geracao,
          qtd_mod: this.qtd_mod,
          pot_sistema: this.pot_sistema,
          co2: this.co2,
          arvores: this.arvores,
          economia_mensal: this.economia_mensal
        }
      } else {
        const errjson = {
          err,
          message: 'Não foi possível efetuar a sua simulação, lamentamos por isto. Nossa equipe já está trabalhando para solucionar este problema.'
        }
        res.json(JSON.stringify(errjson))
      }

      console.log('Objeto Response', JSON.stringify(Response, null, 2))

      let Kit
      CORE
        .SearchProduct(App, {
          "Page": {
            "PageIndex": 0,
            "PageSize": 0
          },
          "Where": "Name.Contains(\"gerador\") == true",
          "WhereMetadata": `SerializedValue.Contains(\"${RoofType}\") == true`,
          "OrderBy": ""
        })
        .then((response) => {
          Kit = _.filter(response.Result, i => (
            Boolean(_.find(i.MetadataValues, m => m.PropertyName === "GeradorFotovoltaicoModulos" && Number(m.Value) >= this.qtd_mod))
            &&
            Boolean(_.sortBy(i, m => m.PropertyName === "GeradorFotovoltaicoModulos"))
          ))

          Response.Kit = {
            Name: Kit[0].Name,
            ProductID: Kit[0].ProductID,
            UrlFriendly: Kit[0].UrlFriendly,
            NexenPesokg: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'NexenPesokg'),
            NexenAlturacm: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'NexenAlturacm'),
            NexenLarguracm: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'NexenLarguracm'),
            NexenProfundidadecm: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'NexenProfundidadecm'),
            GeradorFotovoltaicoPoteciakWp: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'GeradorFotovoltaicoPoteciakWp'),
            GeradorFotovoltaicoModulos: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'GeradorFotovoltaicoModulos'),
            GeradorFotovoltaicoInversores: Kit[0].MetadataValues.find(obj => obj.PropertyName === 'GeradorFotovoltaicoInversores')
          }

          const new_year = new Date()

          Months.forEach((m, i) => {
            this.chart.push(parseFloat(((m * parseInt(Response.Kit.GeradorFotovoltaicoModulos.Value) * 85 * 330 * (this.daysInMonth(++i, new_year.getUTCFullYear()) / 1000)) * parseFloat(Tax.TARIFA.replace(',', '.')).toFixed(4)).toFixed(2)))
          })
          Response.chart = this.chart

          res.json(Response)
          this.chart = []
        })
        .catch((error) => {
          res.json(error)
          console.log(`SearchProduct ${error}`)
        })
    })
  }

  filterArray(array, filters) {
    const filterKeys = Object.keys(filters)
    return array.filter(item => {
      // validates all filter criteria
      return filterKeys.every(key => {
        // ignores non-function predicates
        if (typeof filters[key] !== 'function') return true
        return filters[key](item[key])
      })
    })
  }

  update(req, res, App) {
    if (App.logs) console.time('Update Simulacao')
    if (fs.existsSync(App.lib.path)) fs.unlinkSync(App.lib.path)

    return extractSheets({
      credentials,
      spreadsheetKey: App.lib.spreadsheetKey,
      sheetsToExtract: App.lib.sheetsToExtract
    }, (err, data) => {
      if (err) console.log('ERROR:', err)

      fs.writeFileSync(App.lib.path, JSON.stringify(data), (err) => {
        if (err) throw err
      })

      res.sendFile(path.resolve('success.html'))
    })

    if (App.logs) console.timeEnd('Update Simulacao')
  }

  getDealerships(req, res, App) {
    fs.readFile(App.lib.path, (err, data) => {
      if (err) throw err
      const db = JSON.parse(data)
      let conces = db.Tarifas.map((curr) => curr.CONCESSIONARIA)
      conces = conces.filter((item, index) => conces.indexOf(item) === index)
      res.json(conces.sort())
    })
  }

  getPlaces(req, res, App) {
    fs.readFile(App.lib.path, (err, data) => {
      if (err) throw err
      const db = JSON.parse(data)
      let places = db.Radiacao.map((curr) => fixUtf8(curr.NAME.replace('Ã', 'Á')))
      // places = places.filter((item, index) => places.indexOf(item) === index)
      places = places.filter((item, index) => places.indexOf(req.body.city) === index)
      res.json({
        isValid: places.length > 0 ? true : false,
        places: places,
        request: req.body.city
      })
    })
  }

  totalDays(year) {
    return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0) ? 366 : 365
  }

  average(numbers) {
    let sum = 0
    sum = numbers.reduce(function (a, b) { return a + b })
    return parseFloat((sum / numbers.length).toFixed(2)) || 0 // returns 0 if the value in ( ) evaluates to a falsy value, (i.e. NaN)
  }

  percentage(num, per) {
    return (num / 100) * per
  }

  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }
}